<?php

namespace App\Traits;

use Carbon\Carbon;

trait HelperMethods
{
    public function currentMonth()
    {
        $date = new Carbon('first day of this month');
        $date->hour = 00;
        $date->minute = 00;
        $date->second = 00;
        return $date;
    }
    public function getTasksForThisMonth()
    {
        $tasks = $this->tasks->whereNotIn('pivot_status', ['assigned', 'requested'])->where('assigned_on', '>=', $this->currentMonth());
        return $tasks;
    }

    public function getResolvedTasksForThisMonth()
    {
        $tasks = $this->tasks()->where('completed_at', '>=', $this->currentMonth())->where('tasks.status', 'resolved')->get();
        return $tasks;
    }

    public function calcEfficiency()
    {
        $tasks = $this->getTasksForThisMonth();
        $resolvedTasks = $this->getResolvedTasksForThisMonth();
        $totalTasksCount = $tasks->count();
        $resolvedTasksCount = $resolvedTasks->count();
        //Success Rate Calculation

        $successRate = 0.0;
        if ($totalTasksCount) {
            if ($resolvedTasksCount) {
                $successRate = ($resolvedTasksCount * 60) / $totalTasksCount;
            } else {
                return $successRate;
            }
        } else {
            return $successRate;
        }
        $reassignCount = 0.0;
        $tasks->each(function ($task) use (&$reassignCount) {

            $reassignCount += $task->reassignCount;
        });
        $reassignCount += $totalTasksCount;
        $averageForReassign = ($totalTasksCount / $reassignCount) * 20;

        // Time taken Calculation
        $totalGivenTime = 0.0;
        $totalTakenTime = 0.0;
        $resolvedTasks->each(function ($task) use (&$totalGivenTime, &$totalTakenTime) {
            // getAverageForTimeInterval retrieves the difference between due_date and completed_at and after performing some calculation it returns the result that how much time is saved.
            $average = $task->getAverageForTimeInterval();
            $totalGivenTime += $average['givenTime'];
            $totalTakenTime += $average['takenTime'];
        });
        $averageTimeTaken = ($totalTakenTime / $totalGivenTime) * 20;
        $averageForTime = 20 - $averageTimeTaken;
        $totalAverage = $successRate + $averageForTime + $averageForReassign;
        return $totalAverage;
    }
}
