<?php

namespace App\Policies;

use App\Task;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Str;

class TaskPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function view(User $user, Task $task)
    {
        //
        return (((strtolower($task->status) != 'assigned') && ($user->id === $task->team->leader_id)) || (strtolower($user->role) === 'admin'));
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        return $user->isLeader();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function update(User $user, Task $task)
    {
        //
        $status = (Str::lower($task->status));
        return (($user->id === $task->team->leader_id) && ($status === 'assigned' || $status === 'opened'));
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function delete(User $user, Task $task)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function restore(User $user, Task $task)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function forceDelete(User $user, Task $task)
    {
        //
    }

    public function markAsApproved(User $user, Task $task)
    {
        $member = $task->active_member;
        if ($member) {
            return (($user->id === $task->team->leader_id) && ($member->pivot->status === 'requested'));
        } else
            return false;
    }

    public function reject(User $user, Task $task)
    {
        $member = $task->active_member;
        if ($member)
            return (($user->id === $task->team->leader_id) && ($member->pivot->status === 'requested'));
        else
            return false;
    }

    public function resolveTask(User $user, Task $task)
    {
        $member = $task->active_member;
        return (($user->id === $member->id) && ($member->pivot->status === 'assigned'));
    }

    public function unresolveTask(User $user, Task $task)
    {
        $member = $task->active_member;
        return (($user->id === $member->id) && ($member->pivot->status === 'assigned'));
    }

    public function reassignTask(User $user, Task $task)
    {
        return (($task->due_date > now()) && ($user->id === $task->team->leader_id) && ($task->status === "opened"));
    }
}
