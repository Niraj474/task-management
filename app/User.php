<?php

namespace App;

use App\Traits\HelperMethods;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;
    use HelperMethods;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'team_id', 'status', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    // Relationship Methods

    public function tasks()
    {

        return $this->belongsToMany(Task::class, 'member_task', 'member_id', 'task_id')->withTimestamps()->withPivot('status', 'task_id', 'member_id', 'reassign_count');
    }

    public function assignedTasks()
    {
        $tasks = $this->tasks()->wherePivotIn('status', ['requested', 'assigned'])->get();
        return $this->modifyTaskAccordingToDueDate($tasks);
    }

    public function resolvedTasks()
    {
        $tasks = $this->tasks()->wherePivotIn('status', ['resolved'])->get();
        return $this->modifyTaskAccordingToDueDate($tasks);
    }
    public function unresolvedTasks()
    {
        $tasks = $this->tasks()->wherePivotIn('status', ['unresolved'])->get();
        return $this->modifyTaskAccordingToDueDate($tasks);
    }

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function modifyTaskAccordingToDueDate($tasks)
    {
        if ($tasks->isNotEmpty()) {
            $tasks->each(function ($task) {
                $status = strtolower($task->status);
                if ($task->due_date < now() &&  ($status === 'assigned' || $status === 'opened')) {
                    $activeMember = $task->active_member;
                    unset($task->member);
                    if ($activeMember) {
                        $task->users()->wherePivot('member_id', $activeMember->id)->updateExistingPivot($activeMember->id, ['status' => 'unresolved']);
                        if ($activeMember->assigned_tasks <= 0) {
                            $activeMember->update(['status' => 'vacant']);
                            $activeMember->decrement('assigned_tasks');
                        }
                    }
                    $task->update(['status' => 'unresolved']);
                }
            });
        }
        return $tasks;
    }

    public function scopeAssigned($query)
    {
        return $query->orderBy('assigned_tasks');
    }

    public function getTeamNameAttribute()
    {
        return $this->team ? $this->team->name : null;
    }

    public function getTasks($role)
    {
        $role = Str::lower($role);
        $tasks = [];
        switch ($role) {
            case 'admin':
                $tasks = Task::orderBy('priority', 'desc')->with('team', 'activeTaskMember')->get();
                break;
            case 'leader':
                $tasks = $this->team->tasks()->with('team', 'activeTaskMember')->orderBy('priority', 'desc')->get();
                break;
            case 'member':
                $tasks = $this->tasks()->with('team', 'activeTaskMember')->orderBy('priority', 'desc')->get();
                break;
        }

        return $tasks;
    }

    public function isBusy()
    {
        return Str::lower($this->status) == 'busy' ? true : false;
    }

    public function isAdmin()
    {
        return Str::lower($this->role) === 'admin' ? true : false;
    }

    public function isLeader()
    {
        return Str::lower($this->role) === 'leader' ? true : false;
    }
    public function isMember()
    {
        return Str::lower(auth()->user()->role) === 'member';
    }
}
