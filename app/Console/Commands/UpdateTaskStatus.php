<?php

namespace App\Console\Commands;

use App\Task;
use Illuminate\Console\Command;

class UpdateTaskStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the task of all the status which had crossed the due';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tasks = Task::all();
        if($tasks->isNotEmpty())
        {
            $tasks->each(function($task){
                $status = strtolower($task->status);
                if($task->due_date < now() &&  ($status === 'assigned' || $status === 'opened')){
                    $activeMember = $task->getActiveTaskMember();
                    if($activeMember)
                    {
                        $task->users()->wherePivot('member_id',$activeMember->id)->updateExistingPivot($activeMember->id,['status'=>'unresolved']);
                        if($activeMember->assignedTasks()->count() <= 0)
                        {
                            $activeMember->update(['status'=>'vacant']);
                        }
                    }
                    $task->update(['status'=>'unresolved']);

                }
            });
        }
    }
}
