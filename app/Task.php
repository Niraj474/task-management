<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Task extends Model
{

    protected $fillable = ['status', 'completed_at', 'due_date', 'name', 'team_id', 'assigned_on', 'priority'];

    protected $dates = ['due_date', 'assigned_on', 'completed_at'];

    //Relationship methods
    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'member_task', 'task_id', 'member_id')->withTimestamps()->withPivot('member_id', 'task_id', 'status', 'reassign_count');
    }

    public function getActiveMemberAttribute()
    {
        if (!$this->member) {
            $this->member = $this->activeTaskMember->first();
        }
        return $this->member;
    }

    // public function getStatusAttribute($status)
    // {
    //     if(!auth()->user()->isMember())
    //         return $status;
    //     else
    //         return $this->users()->wherePivot('member_id',auth()->id())->first()->pivot->status;
    // }

    public function activeTaskMember()
    {
        $status = Str::lower($this->status);
        if ($status == 'opened') {
            return null;
        }
        if ($status === 'assigned') {
            return $this->belongsToMany(User::class, 'member_task', 'task_id', 'member_id')->withPivot('id', 'member_id', 'status', 'reassign_count')->orderBy('member_task.updated_at', 'desc');
        } else {
            return $this->belongsToMany(User::class, 'member_task', 'task_id', 'member_id')->withPivot('member_id', 'status', 'reassign_count')->orderBy('member_task.updated_at', 'desc');
        }
    }

    public function isRequested()
    {
        $member = $this->active_member;
        if ($member)
            return (auth()->id() === $member->id) && (Str::lower($member->pivot->status) == 'requested');
        else
            return false;
    }


    public function getCompletedDate()
    {
        if (auth()->user()->isMember())
            return;
        $date = null;
        if ($this->completed_at != null) {
            $date = $this->completed_at->diffForHumans();
        }
        if ($this->due_date < now()) {
            return $date ?? "<span class='text-danger'>Failed</span>";
        } else {
            return $date ?? "<span class='text-warning'>In Progress</span>";
        }
    }

    public function getReassignCountAttribute()
    {
        $reassignCount = 0.0;
        if (!$this->pivot) {
            $records = DB::table('member_task')->where('task_id', $this->id)->pluck('reassign_count');
            $records->each(function ($record) use (&$reassignCount) {
                $reassignCountForIndividualRecord = 0.0;
                $reassignCountForIndividualRecord = explode('_', $record);
                $reassignCountForIndividualRecord = $reassignCountForIndividualRecord[0] + $reassignCountForIndividualRecord[1];
                $reassignCount += $reassignCountForIndividualRecord;
            });
            return $reassignCount;
        }
        $reassignCount = $this->pivot->reassign_count;
        $reassignCount = explode('_', $reassignCount);
        $reassignCount = $reassignCount[0] + $reassignCount[1];
        return $reassignCount;
    }

    public function getAverageForTimeInterval()
    {
        $completed_at = $this->completed_at;
        $due_date = $this->due_date;
        $assigned_on = $this->assigned_on;
        if ($due_date->DiffInHours($assigned_on) > 1) {
            $givenTime = $due_date->floatDiffInHours($assigned_on);
            $takenTime = $due_date->floatDiffInHours($completed_at);
        } else {
            $givenTime = $due_date->floatDiffInMinutes($assigned_on);
            $takenTime = $assigned_on->floatDiffInMinutes($completed_at);
        }
        return ['givenTime' => $givenTime, 'takenTime' => $takenTime];
    }

    public function getPriorityAttribute($value)
    {
        if (($value) === 1)
            return "<span class='bg-info py-1 px-2 text-white'>Low</span>";
        if (($value) === 2)
            return "<span class='bg-warning py-1 px-2 text-white'>Medium</span>";
        if (($value) === 3)
            return "<span class='bg-danger py-1 px-2 text-white'>High</span>";
    }
}
