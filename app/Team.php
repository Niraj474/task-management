<?php

namespace App;

use App\Traits\HelperMethods;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Team extends Model
{
    use HelperMethods;

    protected $fillable = ['leader_id', 'name'];

    //Relationship methods
    public function members()
    {
        return $this->hasMany(User::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function leader()
    {
        return $this->belongsTo(User::class, 'leader_id');
    }

    public function getTasksCount()
    {
        $count = [
            'assigned' => 0,
            'resolved' => 0,
            'unresolved' => 0,
        ];
        $result = $this->tasks()->select(DB::raw('COUNT(*) as count,status'))->where('team_id', $this->id)->groupBy('status')->get()->toArray();

        foreach ($result as $key => $value) {
            $count[Str::lower($value['status'])] = $value['count'];
        }

        return $count;
    }

    public function isTeamMember($id)
    {
        return in_array($id, $this->members()->pluck('id')->toArray()) ? true : false;
    }

    public function assignTaskAutomatically($previousMember = null)
    {
        $members = $this->members->where('role', 'member')->where('status', 'vacant');
        if ($previousMember) {
            $members = $members->whereNotIn('id', [$previousMember->id]);
        }
        if ($members->isEmpty()) {
            $members = $this->members;
            $members = $members->assigned();
            $member = $members->first();
        } else {
            if ($members->count() <= 1) {
                return $members->first();
            }
            $members->each(function ($member) {
                $member->average = $member->calcEfficiency();
            });
            $member = $members->where('average', '>', 0.0)->sortByDesc('average')->first();
            if (!$member) {
                $member = $members->random();
            }
        }
        unset($member->average);
        return $member;
    }
}
