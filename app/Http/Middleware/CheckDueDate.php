<?php

namespace App\Http\Middleware;

use Closure;

class CheckDueDate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->task->due_date > now())
            return $next($request);
        else
        {
            session()->flash('warning', 'You Cant edit the Task Now !');
            return redirect(route('tasks.index'));
        }
    }
}
