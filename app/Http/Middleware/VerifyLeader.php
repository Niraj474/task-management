<?php

namespace App\Http\Middleware;

use App\User;
use Carbon\Carbon;
use Closure;

class VerifyLeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $leader = auth()->user();
        if ($request->task) {
            $leader = $request->task->team->leader;
            $team = $request->task->team;
            $status = strtolower($request->task->status);
            if (($request->task->due_date < Carbon::now()) && ($status == 'assigned' || $status == 'opened')) {
                session()->flash('warning', 'You Cant edit the Task Now !');
                return redirect(route('tasks.index'));
            }
            $task = $request->task;
            if (($task->team_id === $team->id) && (auth()->id() === $leader->id))
                return $next($request);
            else
                return abort(401);
        } elseif (auth()->id() === $leader->id)
            return $next($request);
        else
            return abort(401);
    }
}
