<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class checkUsersCount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $usersCount = User::where('team_id', null)->where('role', 'member')->count();
        if ($usersCount > 1) {
            return $next($request);
        } else {
            session()->flash('error', 'Not enough members are available to form a team');
            return redirect(route('users.create'));
        }
    }
}
