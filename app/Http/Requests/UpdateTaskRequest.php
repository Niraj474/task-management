<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'=>'required|unique:tasks,name,' . $this->task->id,
            'priority'=>'required|integer'
        ];
        // if(!$this->assign){
        //     if(!$this->member){
        //         $rules['member']='required';
        //     }
        // }
        return $rules;
    }
}
