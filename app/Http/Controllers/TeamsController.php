<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTeamRequest;
use App\Http\Requests\UpdateTeamRequest;
use App\Team;
use App\User;
use Illuminate\Http\Request;

class TeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verifyAdmin']);
        $this->middleware('checkUsersCount')->only('create');
    }
    public function index()
    {
        //
        $teams = Team::with('members')->get();
        return view('teams.index')->withTeams($teams);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $users = User::where('team_id', null)->where('role', 'member')->get();
        return view('teams.create')->withUsers($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTeamRequest $request)
    {
        //
        $team = Team::create([
            'name' => $request->name,
            'leader_id' => $request->leader
        ]);
        $team->save();
        $user = User::whereId($request->leader);
        $user->update([
            'role' => 'leader',
            'team_id' => $team->id
        ]);
        $members = $request->members;
        foreach ($members as $key => $member_id) {
            $user = User::whereId($member_id);
            $user->update(['team_id' => $team->id]);
        }
        session()->flash('success', 'Team has been created successfully!');
        return redirect(route('teams.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        //
        $users = User::where('team_id', null)->where('role', 'member')->get();
        return view('teams.edit', ['users' => $users, 'team' => $team]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeamRequest $request, Team $team)
    {
        //
        $members = $request->members;

        $oldTeamMembers = $team->members;

        $oldTeamMembers->each(function ($member) {
            $member->update(['team_id' => null, 'role' => 'member']);
        });

        $newLeader = User::whereId($request->leader)->firstOrFail();
        $newLeader->update([
            'role' => 'leader',
            'team_id' => $team->id
        ]);
        $team->update(['leader_id' => $newLeader->id]);


        foreach ($members as $key => $value) {

            $user = User::whereId($value)->firstOrFail();
            $user->update([
                'team_id' => $team->id,
                'role' => 'member'
            ]);
        }

        session()->flash('success', 'Team has been updated successfully!');
        return redirect(route('teams.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
}
