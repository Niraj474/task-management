<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Notifications\NewTaskAssigned;
use App\Notifications\UpdateTaskStatus;
use App\Task;
use App\Team;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class TasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verifyLeader')->only('create', 'store', 'edit', 'update', 'approve', 'reject');
        $this->middleware('checkDueDate')->only('resolve', 'unresolve', 'reassign');
    }

    public function resolvedTasks()
    {
        $tasks = auth()->user()->resolvedTasks();
        $tasks = $tasks->sortByDesc('priority');
        return view('tasks.index', compact('tasks'));
    }
    public function unresolvedTasks()
    {
        $tasks = auth()->user()->unresolvedTasks();
        $tasks = $tasks->sortByDesc('priority');
        return view('tasks.index', compact('tasks'));
    }
    public function assignedTasks()
    {
        $tasks = auth()->user()->assignedTasks();
        $tasks = $tasks->sortByDesc('priority');
        return view('tasks.index', compact('tasks'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tasks = auth()->user()->getTasks(auth()->user()->role);
        // dd($tasks);
        $tasks = auth()->user()->modifyTaskAccordingToDueDate($tasks);;
        return view('tasks.index')->withTasks($tasks);
    }

    public function show(Task $task)
    {
        if ($this->authorize('view', $task)) {
            return view('tasks.show', compact('task'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $team = auth()->user()->team;
        $members = $team->members()->whereRole('member')->get();
        return view('tasks.create', compact('members', 'team'));
    }

    /**
     * Store a newly created resource in storage.
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTaskRequest $request)
    {
        //
        $team = auth()->user()->team;
        if ($request->assign) {
            $member = $team->assignTaskAutomatically();
        } else {
            $member = User::whereId($request->member)->firstOrFail();
        }
        $task = $team->tasks()->save(Task::create([
            'name' => $request->name,
            'due_date' => $request->due_date,
            'assigned_on' => Carbon::now(),
            'status' => 'opened',
            'priority' => $request->priority
        ]));
        if ($member->status == 'vacant') {
            $member->update(['status' => 'busy']);
        }
        $member->increment('assigned_tasks');
        $task->update(['status' => 'assigned']);
        $task->users()->attach($member->id, ['status' => 'assigned']);
        $member->notify(new NewTaskAssigned($task));
        session()->flash('success', 'Task has been created successfully!');
        return redirect(route('tasks.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
        $members = $task->team->members()->whereRole('member')->get();
        if (Route::is('tasks.edit')) {
            return view('tasks.edit', compact('task', 'members'));
        } else {
            return view('tasks.assign', compact('task', 'members'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
        //
        $task->update([
            'name' => $request->name,
            'due_date' => $request->due_date,
            'priority' => $request->priority
        ]);
        session()->flash('success', 'Task has been updated successfully!');
        return redirect(route('tasks.index'));
    }

    public function resolve(Task $task, User $member)
    {

        if (($task->active_member->id === $member->id) && ($this->authorize('resolveTask', $task))) {
            $task->users()->wherePivot('member_id', $member->id)->updateExistingPivot($member->id, ['status' => 'requested']);
            session()->flash('success', 'Your request has been submitted successfully!');
            return redirect(route('tasks.index'));
        } else {
            abort(401);
        }
    }

    public function unresolve(Task $task, User $member)
    {
        $currentMember = $task->active_member;
        if ($this->authorize('unresolveTask', $task) && ($member->id === $currentMember->id)) {
            unset($task->member);
            if ($currentMember->tasks()->wherePivot('status', 'assigned')->wherePivotNotIn('task_id', [$task->id])->get()->isEmpty()) {
                $currentMember->update([
                    'status' => 'vacant'
                ]);
            }
            $task->users()->wherePivot('member_id', $currentMember->id)->updateExistingPivot($currentMember->id, ['status' => 'unresolved']);

            $task->update(['status' => 'opened']);
            $currentMember->decrement('assigned_tasks');
            session()->flash('warning', 'Task is successfully marked as unresolved!');
            return redirect(route('tasks.index'));
        } else {
            abort(401);
        }
    }

    public function approve(Task $task)
    {
        if ($this->authorize('markAsApproved', $task)) {
            $activeMember = $task->active_member;
            unset($task->member);
            $task->update([
                'status' => 'resolved',
                'completed_at' => Carbon::now()
            ]);
            $task->users()->wherePivot('member_id', $activeMember->id)->updateExistingPivot($activeMember->id, ['status' => 'resolved']);
            $activeMember->update([
                'status' => 'vacant'
            ]);
            $activeMember->decrement('assigned_tasks');
            $activeMember->notify(new UpdateTaskStatus($task, 'approved'));
            session()->flash('success', 'Task has been approved successfully!');
            return redirect(route('tasks.index'));
        } else {
            abort(401);
        }
    }

    public function reject(Task $task)
    {
        if ($this->authorize('reject', $task)) {
            $activeMember = $task->active_member;
            unset($task->member);
            $reassignCount = $activeMember->pivot->reassign_count;
            $reassignCount = explode('_', $reassignCount); //1 index contains rejection counts.
            $reassignCount[1] += 0.25;
            $reassignCount = implode('_', $reassignCount);
            $task->users()->wherePivot('member_id', $activeMember->id)->updateExistingPivot($activeMember->id, ['status' => 'assigned', 'reassign_count' => $reassignCount]);
            $activeMember->notify(new UpdateTaskStatus($task, 'rejected'));
            session()->flash('warning', 'Task has been Rejected successfully!');
            return redirect(route('tasks.index'));
        } else {
            abort(401);
        }
    }

    public function reassign(Task $task)
    {
        if ($this->authorize('reassignTask', $task)) {
            if (Route::is('tasks.assignManually')) {
                $member = User::whereId(request()->member)->first();
                $record = $task->users()->wherePivot('member_id', $member->id)->first();
                if ($record) {
                    $reassignCount = $record->pivot->reassign_count;
                    $reassignCount = explode('_', $reassignCount); //0 index contains reassign counts.
                    $reassignCount[0] += 1;
                    $reassignCount = implode('_', $reassignCount);
                    $task->users()->updateExistingPivot($member->id, ['status' => 'assigned', 'reassign_count' => $reassignCount]);
                } else {
                    $member->tasks()->attach($task->id, ['status' => 'assigned']);
                }
            } else {
                $lastActiveMember = $task->users()->wherePivot('status', 'unresolved')->orderBy('member_task.updated_at')->first();
                if ($lastActiveMember) {
                    $member = $task->team->assignTaskAutomatically($lastActiveMember);
                    $member->tasks()->attach($task->id, ['status' => 'assigned']);
                }
            }
            if ($member->status === 'vacant')
                $member->update(['status' => 'busy']);
            $member->increment('assigned_tasks');
            $task->update(['status' => 'assigned']);
            session()->flash('success', "Task has been assigned to $member->name successfully!");
            return redirect(route('tasks.index'));
        } else {
            abort(401);
        }
    }
}
