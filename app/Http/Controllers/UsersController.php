<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Team;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->only('members', 'calcEfficiency');
        $this->middleware('verifyAdmin')->only('store', 'create');
        $this->middleware('verifyLeader')->only('members');
    }
    public function index()
    {
        //
        if ($this->authorize('viewAny', auth()->user())) {
            $users = User::where('role', '<>', 'admin')->get();
            return view('Users.users')->withUsers($users);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        //
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 'member',
            'status' => 'vacant'
        ]);

        session()->flash('success', 'User has been created successfully');
        return redirect(route('users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
        $tasks = $user->tasks;
        return view('Users.show', compact('tasks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function calcEfficiency()
    {
        $users = User::where('role', 'member')->get();
        $users->each(function ($user) {
            $average = $user->calcEfficiency();
            $user->average = $average;
        });
        $teams = Team::all();
        $teams->each(function ($team) {
            $average = $team->calcEfficiency();
            $team->average = $average;
        });

        $user = $users->sortByDesc('average')->first();
        $team = $teams->sortByDesc('average')->first();
        $leader = $team->leader;
        $leader->average = $team->average;
        if (!$user->average > 0)
            $user = null;
        if (!$leader->average > 0)
            $leader = null;
        return view('index', compact('user', 'leader'));
    }

    public function members()
    {
        $team = auth()->user()->team;
        if ($this->authorize('viewMembers', $team)) {
            $team = $team->load('members');
            return view('users.members', compact('team'));
        }
    }
}
