@extends('layouts.layout')
@section('content')
    <div class="container mt-4">
        <div class="card">
            <div class="card-header">
                <h5>Assign Task Manually</h5>
            </div>
        <form class="form" method="POST" action="{{ route('tasks.assignManually',$task->id) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="member">Assign To</label>
                        <select name="member" id="member" class="form-control" required>
                            <option value="" disabled selected>Select Member</option>
                            @foreach ($members as $member)
                                <option value="{{ $member->id }}" {{ old('member') ? 'selected' : '' }}>{{ $member->name }}</option>
                            @endforeach
                        </select>
                        @error('member')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button  class="btn btn-success"type="submit">Assign</button>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
