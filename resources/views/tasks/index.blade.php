@extends('layouts.layout')


@section('content')
    <div class="container mt-4 mb-4">
        @include('includes.message')
        @if (auth()->user()->isMember())
            <ul class="nav nav-tabs d-flex justify-content-center py-3">
            <li class="nav-item"><a href="{{route('tasks.index')}}" class="nav-link {{ Request::is('tasks') ? 'active' : '' }}">All Tasks</a></li>
                <li class="nav-item"><a href="{{route('tasks.assigned')}}" class="nav-link {{ Request::is('tasks/assigned') ? 'active' : '' }}">Assigned Tasks</a></li>
                <li class="nav-item"><a href="{{route('tasks.resolved')}}" class="nav-link {{ Request::is('tasks/resolved') ? 'active' : '' }}">Resolved Tasks</a></li>
                <li class="nav-item"><a href="{{route('tasks.unresolved')}}" class="nav-link {{ Request::is('tasks/unresolved') ? 'active' : '' }}">Unresolved Tasks</a></li>
            </ul>
        @endif
        <h2>Tasks</h2>
        <div class="mt-3">
            <div class="d-flex justify-content-end mb-4">
                @can('create',App\Task::class)
                    <a href="{{ route('tasks.create',auth()->user()->team->name) }}" class="btn btn-primary btn-sm">Create Task</a>
                @endcan
            </div>
            <table class="table">
                @if ($tasks->isNotEmpty())
                    <thead>
                        <td>#</td>
                        <td>Task Name</td>
                        <td>Assigned to</td>
                        <td>Status</td>
                        <td>Priority</td>
                        <td>Team</td>
                        <td>Due Date</td>
                        <td>Assigned on</td>
                        @if (!auth()->user()->isMember())
                            <td>Completed At</td>
                        @endif
                        <td>Actions</td>
                    </thead>
                    <?php
                        $i = 1;
                    ?>
                @endif
                @forelse ($tasks as $task)
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $task->name }}</td>
                        <?php
                        $member = (auth()->user()->isMember() ? \App\User::whereId($task->pivot->member_id)->first() : ($task->active_member));
                        if($member)
                        {
                            if(($member->pivot->status === 'unresolved') && ($task->status === 'opened' || $task->status === 'unresolved')){
                                $memberName = null;
                            }else{
                                $memberName = $member->name;

                            }

                        }
                        ?>
                        <td>{!! $memberName ?? "<span class='text-danger'>No One</span>" !!}</td>
                        <td>{{ auth()->user()->isMember() ? $task->users()->wherePivot('member_id',auth()->id())->first()->pivot->status : Str::ucfirst($task->status) }}</td>
                        <td>{!! $task->priority !!}</td>
                        <td>{{ $task->team->name }}</td>
                        <td>{{ $task->due_date->diffForHumans() }}</td>
                        <td>{{  $task->assigned_on->diffForHumans() }}</td>
                        @if (!auth()->user()->isMember())
                            <td>{!! $task->getCompletedDate() !!}</td>
                        @endif
                        @if (auth()->user()->can('update',$task))
                            <td class="d-flex justify-content-center">
                                @can('update', $task)
                                    <form action="{{ route('tasks.edit',[$task->id]) }}" method="get">
                                        <button class="btn btn-sm btn-outline-primary mr-1">Edit</button>
                                    </form>
                                @endcan
                                @can('markAsApproved', $task)
                                    <form action="{{ route('tasks.approve',$task->id) }}" method="post" class="mr-1">
                                        @csrf
                                        @method('PUT')
                                        <button class="btn btn-sm btn-outline-success">Approve</button>
                                    </form>
                                @endcan
                                @can('reject', $task)
                                    <form action="{{ route('tasks.reject',$task->id) }}" method="post">
                                        @csrf
                                        @method('PUT')
                                        <button class="btn btn-sm btn-outline-danger">Reject</button>
                                    </form>
                                @endcan
                                @can('reassignTask', $task)
                                <form action="{{ route('tasks.reassign',$task->id) }}" method="post" class="ml-2">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-sm btn-outline-secondary">Reassign To Anyone</button>
                                </form>
                                <a href="{{ route('tasks.assign',$task->id) }}" class="btn btn-outline-info btn-sm ml-2">Assign Manually</a>
                                @endcan
                            </td>
                        @else
                            @can('view', $task)
                            <td>
                                <form action="{{ route('tasks.show',$task->id) }}" method="get">
                                    <button class="btn btn-sm btn-outline-primary mr-1">View Details</button>
                                </form>
                            </td>
                            @endcan
                        @endif
                        @if ($task->isRequested())
                            <td>
                                <p class="text-center">Requested</p>
                            </td>
                        @endif
                        <?php
                            $activeMember = $task->active_member;
                        ?>
                        @if ($activeMember && (auth()->id() === $activeMember->id))
                            <td class="d-flex flex-column justify-content-around">
                                @can('resolveTask', $task)
                                <form action="{{ route('tasks.resolve',[$task->id,auth()->id()]) }}" method="post" class="mb-2">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-sm btn-outline-success">Request To Resolve</button>
                                </form>
                                @endcan
                                @can('unresolveTask', $task)
                                    <form action="{{ route('tasks.unresolve',[$task->id,auth()->id()]) }}" method="post">
                                        @csrf
                                        @method('PUT')
                                        <button class="btn btn-sm btn-outline-danger">Unresolve</button>
                                    </form>
                                @endcan
                            </td>
                        @endif
                    </tr>
                    <?php $i++;?>
                @empty
                    <h2 class="text-center">No Tasks</h2>
                @endforelse
            </table>
        </div>
    </div>
@endsection
