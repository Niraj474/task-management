@extends('layouts.layout')

@section('content')
    <div class="container mt-4 mb-4">
        <table class="table table-striped">
            <thead>
                <th>#</th>
                <th>Member</th>
                <th>Assigned On</th>
                <th>Status</th>
                <th></th>
            </thead>
            <?php $i = 1 ?>
            @foreach ($task->users as $member)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $member->name }}</td>
                <td>{{ $member->pivot->created_at->diffForHumans() }}</td>
                <td>{{ $member->pivot->status }}</td>
                <td>
                    <form action="{{ route('users.show',$member->id) }}" method="get">
                        <button class="btn btn-sm btn-outline-primary">View Member Details</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
@endsection
