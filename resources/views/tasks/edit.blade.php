@extends('layouts.layout')
@section('content')
    <div class="container mt-4">
        <div class="card">
            <div class="card-header">
                <h5>Edit Task</h5>
            </div>
            <form action="{{ route('tasks.update',[$task->team->name,$task->id]) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Task Name</label>
                        <input type="text" name="name" id="" class="form-control @error('name') is-invalid @enderror " value="{{ old('name',$task->name) }}" />
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="priority">Task Severity</label>
                        <input type="number" min="1" max="3" name="priority" id="" class="form-control @error('priority') is-invalid @enderror " value="{{ old('priority',$task->priority) }}" />
                        @error('priority')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="due-date">Due Date</label>
                        <input type="text" name="due_date" id="due_date" class="form-control" value="{{ old('due_date',$task->due_date) }}">
                        @error('due_date')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    {{-- <div class="form-group">
                        <label for="member">Assign To</label>
                        <select name="member" id="member" class="form-control" {{ old('assign') ? 'disabled' : '' }}>
                            @foreach ($members as $member)
                                <option value="{{ $member->id }}" {{ old('member') == $member->id ? 'selected' : ($member->id == $task->getActiveTaskMember()->id ? 'selected' : '') }}>{{ $member->name }}</option>
                            @endforeach
                        </select>
                        @error('member')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" id="assign" type="checkbox"  {{ old('assign') ? 'checked' : '' }} name="assign" />
                            <label class="custom-control-label" for="assign">Assign Task Automatically</label>
                        </div>
                    </div> --}}
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        const date = new Date();
        date.setMinutes(date.getMinutes() + 20);
        flatpickr('#due_date',{
            'minDate': date,
            'enableTime':true,
        });

        const member = $('#member');
        const assign = document.getElementById('assign');

        assign.addEventListener('click',(event)=>{
            member.attr('disabled',assign.checked);
        });

    </script>
@endsection
