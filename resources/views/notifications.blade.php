@extends('layouts.layout')

@section('content')
<div class="container mt-4 mb-4">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>Notifications</h3>
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        @forelse ($notifications as $notification)
                            <li class="list-group-item">
                                @if ($notification->type === 'App\\Notifications\\UpdateTaskStatus')
                                    <div class="d-flex justify-content-between">
                                        <p class="mb-0">Your submission of the task has been {{ $notification->data['status'] }} by the leader of your team</p>
                                        <?php
                                            $status = $notification->data['task']['status'];
                                            $status = Str::lower($status);
                                            $route = 'tasks.'.$status;
                                        ?>
                                        <a href="{{ route($route) }}">View Task</a>
                                    </div>
                                @endif
                                @if ($notification->type === 'App\\Notifications\\NewTaskAssigned')
                                    <div class="d-flex justify-content-between">
                                        <p class="mb-0">A new Task has been Assigned To You</p>
                                        <?php
                                            $status = $notification->data['task']['status'];
                                            $status = Str::lower($status);
                                            $route = 'tasks.'.$status;
                                        ?>
                                        <a href="{{ route($route) }}">View Task</a>
                                    </div>
                                @endif
                            </li>
                        @empty
                            <h5>No Notifications</h5>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
