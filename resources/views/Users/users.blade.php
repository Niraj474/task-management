@extends('layouts.layout')

@section('page')
<?php
    $page = 'users'
?>
@endsection

@section('content')
    <div class="container mt-4 mb-4">
        <h2>Users</h2>
        <div class="mt-3">
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Team</th>
                    <th>Role</th>
                </thead>
                <?php
                    $i = 1;
                ?>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td class="{{ $user->teamName ? '' : 'text-danger' }}">{{ $user->teamName ?? 'Currently not assigned' }}</td>
                        <td>{{ $user->role  }}</td>
                        <td>
                            <a href="{{ route('users.show',$user) }}" class="btn btn-outline-primary btn-sm">View Details</a>
                        </td>
                    </tr>
                    <?php $i++;?>
                @endforeach
            </table>
        </div>
    </div>
@endsection
