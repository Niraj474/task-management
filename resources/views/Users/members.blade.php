@extends('layouts.layout')

@section('page')
<?php
    $page = 'users'
?>
@endsection

@section('content')
    <div class="container mt-4 mb-4">
        <div class="mb-4">
            <div class="alert alert-info">
                <div class="text-muted">{{ $team->name }}</div>
            </div>
        </div>
        <h2>Team Members</h2>
        <div class="mt-3">
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th></th>
                </thead>
                <?php
                    $i = 1;
                ?>
                @foreach ($team->members as $member)
                    @if ($member->role === 'member')
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $member->name }}</td>
                            <td>{{ $member->email }}</td>
                            <td>
                                <a href="{{ route('users.show',$member) }}" class="btn btn-outline-primary">View Details</a>
                            </td>
                        </tr>
                        <?php $i++;?>
                    @endif
                @endforeach
            </table>
        </div>
    </div>
@endsection
