@extends('layouts.layout')

@section('content')
    <div class="container mt-4 mb-4">
        <table class="table table-striped">
            <thead>
                <th>#</th>
                <th>Task</th>
                <th>Assigned On</th>
                <th>Status</th>
                <th></th>
            </thead>
            <?php $i = 1 ?>
            @forelse ($tasks as $task)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $task->name }}</td>
                <td>{{ $task->pivot->created_at->diffForHumans() }}</td>
                <td>{{ $task->pivot->status }}</td>
                <td>
                    <form action="{{ route('tasks.show',$task->id) }}" method="get">
                        <button class="btn btn-sm btn-outline-primary">View Task Details</button>
                    </form>
                </td>
            </tr>
            @empty
                <h5>No Tasks</h5>
            @endforelse
        </table>
    </div>
@endsection
