@extends('layouts.layout')

@section('content')
    <div class="container mt-3">
        <div class="card">
            <div class="card-header">
                <h5>Create User</h5>
            </div>
            <form action="{{ route('users.store') }}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control  @error('name') is-invalid @enderror" name="name" placeholder="User's Name" value="{{ old('name') }}" />
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control  @error('email') is-invalid @enderror" name="email" placeholder="User's Email" value="{{ old('email') }}" />
                        @error('email')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control  @error('password') is-invalid @enderror" name="password" placeholder="" />
                        @error('password')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="" for="inputConfirmPassword">Confirm Password</label>
                        <input class="form-control" id="inputConfirmPassword" type="password" placeholder="" name="password_confirmation" />
                        @error('password')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <button type="submit" class="btn btn-success">Create</button>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
