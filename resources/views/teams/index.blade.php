@extends('layouts.layout')


@section('content')
    <div class="container mt-4 mb-4">
        @include('includes.message')
        <div class="mt-3">
            <h2>Teams</h2>
            <div class="d-flex justify-content-between mb-3">
                <p></p>
                <a href="{{ route('teams.create') }}" class="btn btn-sm btn-primary">Add Team</a>
            </div>
            <table class="table table-striped">
                <thead>
                    <td>#</td>
                    <td>Name</td>
                    <td>Leader</td>
                    <td>No. of Members</td>
                    <td>Completed Tasks</td>
                    <td>Failed Tasks</td>
                    <td>Active Tasks</td>
                    <td>Actions</td>
                </thead>
                <?php
                    $i = 1;
                ?>
                @foreach ($teams as $team)
                <?php
                    $tasksCount = $team->getTasksCount();
                ?>
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $team->name }}</td>
                        <td>{{ $team->leader->name }}</td>
                        <td>{{ $team->members->count()-1 }}</td>
                        <td>{{ $tasksCount['resolved'] }}</td>
                        <td>{{ $tasksCount['unresolved'] }}</td>
                        <td>{{ $tasksCount['assigned'] }}</td>
                        <td>
                            <a  href="{{ route('teams.edit',$team->id) }}" class="btn btn-outline-primary btn-sm mr-2">Edit</a>
                        </td>
                    </tr>
                    <?php $i++;?>
                @endforeach
            </table>
        </div>
    </div>
@endsection
