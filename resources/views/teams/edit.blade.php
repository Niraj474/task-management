@extends('layouts.layout')

@section('content')
    <div class="container mt-3">
        <div class="card">
            <div class="card-header">
                <h5>Edit Team</h5>
            </div>
            <form action="{{ route('teams.update',$team->id) }}" method="post" id="updateForm">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control  @error('name') is-invalid @enderror" name="name" placeholder="Team Name" value="{{ old('name',$team->name) }}" />
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="leader">Leader</label>
                        <select name="leader" id="leader" class="form-control  @error('leader') is-invalid @enderror" onchange="tp({{ $users }},{{ $team->members }},{{ $team->leader }})">
                            @foreach ($team->members as $member)
                                @if (!$member->isBusy())
                                    <option {{ $team->leader->id==$member->id ? 'selected' : '' }} value="{{ $member->id }}">{{ $member->name }}</option>
                                @endif
                            @endforeach
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                        @error('leader')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="members">Team Members</label>
                        <select name="members[]" id="members" class="form-control" multiple>
                            @foreach ($team->members as $member)
                                @if (!(Str::lower($member->role) == 'leader'))
                                    <option value="{{ $member->id }}" {{ $member->isBusy() ? 'disabled selected' : 'selected' }} class="py-1">{{ $member->name }} {{ $member->isBusy() ? '(Task in Process)' : '' }}</option>
                                @endif
                            @endforeach
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}"  class="py-1">{{ $user->name }}</option>
                            @endforeach
                        </select>
                        @error('members')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        const form = document.getElementById('updateForm');
        const members = document.getElementById('members');
        const tp = (users,teamMembers,leader) => {
            const selectedUser = $('#leader')[0].value;
            members.innerHTML = "";
            if(selectedUser == leader.id){
                teamMembers.map(user=>{
                    if(user.id != leader.id){
                        const option = document.createElement('option');
                        let optionDisplayValue = user.name;
                        if(user.status.toLowerCase() === 'busy')
                        {
                            optionDisplayValue += ' (Task in process)';
                            option.setAttribute('disabled',true);
                            option.setAttribute('selected',true);

                        }
                        option.setAttribute('value',user.id);
                        option.innerHTML = optionDisplayValue;
                        members.appendChild(option);
                    }
                });
                users.map(user=>{
                    const option = document.createElement('option');
                    option.setAttribute('value',user.id);
                    option.innerHTML = user.name;
                    members.appendChild(option);
                });
            }else{
                teamMembers.map(user=>{
                        if(user.id != selectedUser){
                            const option = document.createElement('option');
                            let optionDisplayValue = user.name;
                            if(user.status.toLowerCase() == 'busy')
                            {
                                optionDisplayValue += ' (Task in process)';
                                option.setAttribute('disabled',true);
                                option.setAttribute('selected',true);
                            }
                            option.setAttribute('value',user.id);
                            option.innerHTML = optionDisplayValue;
                            members.appendChild(option);
                        }
                });
                users.map(user=>{
                    if(user.id != selectedUser){
                    const option = document.createElement('option');
                    option.setAttribute('value',user.id);
                    option.innerHTML = user.name;
                    members.appendChild(option);
                }
            });
            }
        }

        $('#updateForm').submit(function() {
           const options = Array.apply(null,members.options);
           options.map(option=>{
                if(option.disabled){
                    option.disabled = false;
                }
           });
        });

    </script>
@endsection
