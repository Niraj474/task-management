@extends('layouts.layout')

@section('content')
    <div class="container mt-3">
        <div class="card">
            <div class="card-header">
                <h5>Create Team</h5>
            </div>
            <form action="{{ route('teams.store') }}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control  @error('name') is-invalid @enderror" name="name" placeholder="Team Name" value="{{ old('name') }}" />
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="leader">Leader</label>
                        <select name="leader" id="leader" class="form-control  @error('leader') is-invalid @enderror" onchange="tp({{ $users }})">
                            <option selected disabled value="">Select Leader</option>
                            @foreach ($users as $user)
                            <option value="{{ $user->id }}"  >{{ $user->name }}</option>
                            @endforeach
                        </select>
                        @error('leader')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="members">Team Members</label>
                        <select name="members[]" id="members" class="form-control" multiple>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                        @error('members')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        const tp = (users) => {
            const selectedUser = $('#leader')[0].value;
            const members = document.getElementById('members');
            members.innerHTML = "";
            users.map(user=>{
                if(user.id != selectedUser){
                    const option = document.createElement('option');
                    option.setAttribute('value',user.id);
                    option.innerHTML = user.name;
                    members.appendChild(option);
                }
            });
        }

    </script>
@endsection
