
<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Core</div>
                <a class="nav-link{{ Request::is('dashboard') ? 'active' : '' }}" href="{{ route('dashboard') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>
                <div class="sb-sidenav-menu-heading">Interface</div>
                @if (auth()->user()->isAdmin())
                    <a class="nav-link {{ Request::is('users') ? 'active' : '' }} {{ Request::is('users*') ? '' : 'collapsed' }}" href="{{ route('users.index') }}" data-target="#collapseUsers" data-toggle="collapse" aria-expanded="false" aria-controls="collapseLayouts">
                        <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                        Users
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse {{ Request::is('users*') ? 'show' : '' }}" id="collapseUsers" aria-labelledby="headingOne">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="nav-link {{ Request::is('users/create') ? 'active' : '' }}" href="{{ route('users.create') }}">Create User</a>
                            <a class="nav-link {{ Request::is('users') ? 'active' : '' }} " href="{{ route('users.index') }}">View Users</a>
                        </nav>
                    </div>
                    <a class="nav-link {{ Request::is('teams*') ? '' : 'collapsed'  }} {{ Request::is('teams*') ? 'active' : ''  }} " href="#" data-target="#collapseTeams" data-toggle="collapse">
                        <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                        Teams
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse {{ Request::is('teams*') ? 'show' : '' }}" id="collapseTeams" aria-labelledby="headingOne">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="nav-link {{ Request::is('teams/create') ? 'active' : '' }}" href="{{ route('teams.create') }}">Create Team</a>
                            <a class="nav-link {{ Request::is('teams') ? 'active' : '' }} " href="{{ route('teams.index') }}">View Teams</a>
                        </nav>
                    </div>
                @endif

                @if (auth()->user()->isLeader())
                    <a class="nav-link {{ Request::is('members') ? 'active' : '' }}" href="{{ route('members',auth()->user()->team->name) }}" aria-expanded="false" aria-controls="collapseLayouts">
                        <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                        Members
                    </a>
                @endif


                <a class="nav-link  {{ Request::is('*tasks*') ? '' : 'collapsed'  }} {{ Request::is('*tasks*') ? 'active' : '' }}" href="{{ (!auth()->user()->isLeader()) ? route('tasks.index') : '#' }}" {{ (!auth()->user()->isLeader()) ? '' : "data-toggle=collapse" }} data-target="#collapseTasks" aria-expanded="false" aria-controls="collapseTasks">
                    <div class="sb-nav-link-icon"><i class="fas fa-tasks"></i></div>
                    Tasks
                    @if (auth()->user()->isLeader())
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    @endif
                </a>
                @if (auth()->user()->isLeader())
                    <div class="collapse {{ Request::is('*tasks*') ? 'show' : '' }}" id="collapseTasks" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="nav-link  {{ Request::is('tasks.create') ? 'active' : '' }}" href="{{ route('tasks.create',auth()->user()->team->name) }}">Create Task</a>
                            <a class="nav-link  {{ Request::is('tasks.index') ? 'active' : '' }}" href="{{route('tasks.index')}}">View Tasks</a>
                        </nav>
                    </div>
                @endif

        </div>
    </nav>
</div>
