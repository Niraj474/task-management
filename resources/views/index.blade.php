@extends('layouts.layout')

@section('content')
    <div class="container mt-4 mb-4">
        <h3>Dashboard</h3>
        <div class="mt-4">
            <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item-active">{{ Str::ucfirst(auth()->user()->role) }} Dashboard</li>
        </ol>
        <div class="row">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h6>Employee of the month</h6>
                    </div>
                    <div class="card-body text-center">
                        @if ($user)
                            <span><i class="fas fa-medal fa-5x text-warning"></i></span>
                            <h4 class="text-muted mt-3">{{ $user->name  }}</h4>
                        @else
                            <h3 class="text-muted">No One</h3>
                        @endif
                    </div>
                    <div class="card-footer text-center">
                        @if ($user)
                            <h5 class="text-muted mt-3">Success Rate : {{ round($user->average,2) }}%</h5>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h6>Leader of the month</h6>
                    </div>
                    <div class="card-body text-center">
                        @if ($leader)
                            <span class="text-warning"><i class="fas fa-user-tie fa-5x"></i></span>
                            <h4 class="text-muted mt-3">{{ $leader->name  }}</h4>
                        @else
                            <h3 class="text-muted">No One</h3>
                        @endif
                    </div>
                    <div class="card-footer text-center">
                        @if ($leader)
                            <h5 class="text-muted mt-3">Success Rate : {{ round($leader->average,2) }}%</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
