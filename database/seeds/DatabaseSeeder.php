<?php

use App\Task;
use App\Team;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $admin = factory(User::class)->make();
        $admin->role="admin";
        $admin->email="test@test.com";
        $admin->password = Hash::make('abcd1234');
        $admin->save();

        $users = factory(User::class,rand(20,25))->create();

        $teams = factory(Team::class,rand(3,4))->create();

        $teams->each(function($team) use($users){

            $leader = $users->where('team_id',null)->random();

            $team->update(['leader_id'=>$leader->id]);

            $leader->update(['team_id'=>$team->id,'role'=>'leader']);

            for($i=0;$i<=rand(2,4);$i++){

                $member = $users->where('team_id',null)->random();
                $member->update(['team_id'=>$team->id]);
            }
        });

        $tasks = factory(Task::class,rand(7,10))->create();

        $tasks->each(function($task) use($teams){

            $status = "assigned";

            $team = $teams->random();

            $member = $team->members()->whereRole('member')->get()->random();
            $member->tasks()->attach($task->id,['status'=>$status]);
            $member->update(['status'=>'busy']);

            $task->update(['status'=>$status,'team_id'=>$team->id]);
        });

    }
}
