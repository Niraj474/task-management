<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        //
        'name'=>$faker->sentence(),
        'due_date'=>$faker->dateTimeBetween('today','+1 month'),
    ];
});
