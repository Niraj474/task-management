<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UsersController@calcEfficiency')->name('dashboard');

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/index', function () {
    return view('index');
});

Route::resource('users', 'UsersController');
Route::resource('teams', 'TeamsController');
Route::get('/tasks', 'TasksController@index')->name('tasks.index');
Route::get('/tasks/resolved', 'TasksController@resolvedTasks')->name('tasks.resolved');
Route::get('/tasks/unresolved', 'TasksController@unresolvedTasks')->name('tasks.unresolved');
Route::get('/tasks/assigned', 'TasksController@assignedTasks')->name('tasks.assigned');

Route::get('/tasks/create', 'TasksController@create')->name('tasks.create');
Route::post('/tasks', 'TasksController@store')->name('tasks.store');
Route::get('tasks/{task}/edit', 'TasksController@edit')->name('tasks.edit');
Route::put('/tasks/{task}', 'TasksController@update')->name('tasks.update');
Route::put('/tasks/{task}/{member}/resolve', 'TasksController@resolve')->name('tasks.resolve');
Route::put('/tasks/{task}/{member}/unresolve', 'TasksController@unresolve')->name('tasks.unresolve');
Route::put('/tasks/{task}/approve', 'TasksController@approve')->name('tasks.approve');
Route::put('/tasks/{task}/reject', 'TasksController@reject')->name('tasks.reject');
Route::get('/tasks/{task}', 'TasksController@show')->name('tasks.show');
Route::get('/users/{user}', 'UsersController@show')->name('users.show');
Route::get('/members', 'UsersController@members')->name('members');
Route::get('/notifications', 'NotificationsController@index')->name('notifications')->middleware('auth');
Route::put('/tasks/{task}/reassign', 'TasksController@reassign')->name('tasks.reassign');
Route::get('/tasks/{task}/assign', 'TasksController@edit')->name('tasks.assign');
Route::put('/tasks/{task}/assign', 'TasksController@reassign')->name('tasks.assignManually');
